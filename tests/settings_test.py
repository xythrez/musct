import argparse
import os
import pathlib
import shutil
import unittest

from musct import args
from musct import settings


class SettingsTestCase(unittest.TestCase):
    _base_dir = "tmp"
    _abs_base = os.path.abspath(_base_dir)

    def setUp(self):
        os.makedirs(self._base_dir, exist_ok=True)
        self.file = pathlib.Path(os.path.join(self._base_dir, "some_file"))
        self.exe = pathlib.Path(os.path.join(self._base_dir, "some_exe"))
        self.dir = pathlib.Path(os.path.join(self._base_dir, "some_dir"))
        self.air = pathlib.Path(os.path.join(self._base_dir, "just_air"))
        self.file.touch(mode=0o666)
        self.exe.touch(mode=0o777)
        self.dir.mkdir()

    def tearDown(self):
        shutil.rmtree(self._base_dir, True)

    def test_dir(self):
        absdir = os.path.abspath(self.dir)
        dir = str(self.dir)
        file = str(self.file)
        exe = str(self.exe)
        air = str(self.air)
        self.assertEqual(args._dir(dir), absdir)
        self.assertEqual(args._dir(absdir), absdir)
        with self.assertRaises(argparse.ArgumentTypeError):
            args._dir(air)
        with self.assertRaises(argparse.ArgumentTypeError):
            args._dir(exe)
        with self.assertRaises(argparse.ArgumentTypeError):
            args._dir(file)

    def test_file(self):
        absfile = os.path.abspath(self.file)
        dir = str(self.dir)
        file = str(self.file)
        exe = str(self.exe)
        air = str(self.air)
        self.assertEqual(args._file(file), absfile)
        self.assertEqual(args._file(absfile), absfile)
        self.assertEqual(args._file(exe), os.path.abspath(exe))
        with self.assertRaises(argparse.ArgumentTypeError):
            args._file(air)
        with self.assertRaises(argparse.ArgumentTypeError):
            args._file(dir)

    def test_exe(self):
        absexe = os.path.abspath(self.exe)
        dir = str(self.dir)
        file = str(self.file)
        exe = str(self.exe)
        bin_exe = "bash"
        air = str(self.air)
        self.assertEqual(args._executable(exe), absexe)
        self.assertEqual(args._executable(absexe), absexe)
        self.assertEqual(args._executable(bin_exe), "/usr/bin/bash")
        with self.assertRaises(argparse.ArgumentTypeError):
            args._executable(air)
        with self.assertRaises(argparse.ArgumentTypeError):
            args._executable(file)
        with self.assertRaises(argparse.ArgumentTypeError):
            args._executable(dir)

    # Only tests file generation, the rest is in yaml_test
    def test_load_config(self):
        empty_path = os.path.join(self._abs_base, "gen")
        settings._load_config(empty_path)
        assert os.path.isfile(empty_path) is True

    def test_update_config(self):
        not_dict = ["never", "gonna", "give", "you", "up", "never", "gonna", "let", "you", "down"]
        broken_dict = {"verbose": "YES"}
        file_dict = {
            "verbose": True,
            "force": True,
            "editor": "butterflies",
            "ignore_this": True
        }
        arg_dict = {
            "editor": "Microsoft Word 2000",
            "also_ignore_this": True
        }
        result = settings._update_config(file_dict, arg_dict)
        self.assertTrue(result["verbose"])
        self.assertFalse(result["force"])
        self.assertEqual(result["editor"], "Microsoft Word 2000")
        assert "ignore_this" not in result
        assert "also_ignore_this" not in result
        with self.assertRaises(SystemExit):
            settings._update_config(not_dict, arg_dict)
        with self.assertRaises(SystemExit):
            settings._update_config(broken_dict, arg_dict)

    def test_find_executable(self):
        absexe = os.path.abspath(self.exe)
        dir = str(self.dir)
        file = str(self.file)
        exe = str(self.exe)
        bin_exe = "bash"
        air = str(self.air)
        self.assertEqual(settings._find_executable(exe), absexe)
        self.assertEqual(settings._find_executable(absexe), absexe)
        self.assertEqual(settings._find_executable(bin_exe), "/usr/bin/bash")
        with self.assertRaises(SystemExit):
            settings._find_executable(air)
        with self.assertRaises(SystemExit):
            settings._find_executable(file)
        with self.assertRaises(SystemExit):
            settings._find_executable(dir)


if __name__ == "__main__":
    unittest.main(warnings='ignore')