Musct - Multiple User Settings Configuration Tool
=================================================

**MUSCT** (Pronounced same as "Musket") is a simple configuration unpacker written in Python. It is designed to simplify the process of applying a set of dotfiles to another device. Musct was inspired by [this](http://np.reddit.com/5llx4k) post on **r/unixporn**

Musct reads a `.MUSCTINFO` file from a directory to find available config directories (as packages) within it. It then asks the user to specify which packages to install and applies them based on the information provided in the `.MUSCTPKG` file in each of these packages.

In addition to applying config files, Musct is also capable (to an extent) of installing service files, icon themes, and such, substituting usernames in plaintext files as well as backing up older configs as archives.

Features
--------

* Install configs from specified dotfiles directory
* Install configuration to root owned location
* Backup existing configs into tape archive
* Check config dependencies
* Replace username strings in plaintext configs

Getting Started
---------------
See the [project wiki page](https://gitlab.com/xythrez/musct/wikis/home).

License
-------

Musct is licensed under the GNU GPLv3 license.
