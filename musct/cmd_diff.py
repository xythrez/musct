from musct.cli_command import CliFunction, parse_package_selection
from musct.fileutils import find_conflicting
from musct.printer import BufferedPrinter
from musct.settings import Settings

class DiffFunction(CliFunction):
    def execute(self, avail_pkgs: list, settings: Settings):
        packages = parse_package_selection(avail_pkgs, settings, action="diff")

        printer = BufferedPrinter()

        conflicts = {}
        conflicts_root = {}

        printer.print_line(":: Searching for conflicting files. This may take a while")
        for package in packages:
            files, files_root = package.apply()
            for dest, source in files.items():
                diff = find_conflicting(dest, source)
                conflicts.update(diff)
            for dest, source in files_root.items():
                diff = find_conflicting(dest, source)
                conflicts_root.update(diff)

        if not conflicts and not conflicts_root:
            printer.print_line("No conflicts found.")
        else:
            printer.add("The following conflicts have been found:")
            for dest, source in conflicts.items():
                printer.add(dest)
            for dest, source in conflicts_root.items():
                printer.add("%s (Root)" % dest)
            printer.print()

        return ""
